# EQUIPO 3 


#     -CAROLINA GONZALEZ
#     -CARLOS SCHMIDT
#     -JOHAN CONTRERAS

# INSTALACION DE LIBRERIAS

install.packages('nycflights13')
install.packages('ggplot2')
install.packages('dplyr')
install.packages('lubridate')

# ACTIVACION DE LIBRERIAS

library(nycflights13)
library(ggplot2)
library(dplyr)
library(lubridate)


# RESOLUCION DE CASOS


#1¿Cuál fue el peor día para viajar en 2013? 24/09/2013
head(flights %>%
       group_by(year, month, day) %>%
       summarise(retrasos=sum(dep_delay,na.rm=TRUE)) %>%
       arrange(retrasos),1)


#2  ¿Cuál es la peor aerolinea con respecto a retrasos? EV
head(flights %>%
       rename( Aerolinea = carrier) %>%
       group_by(Aerolinea) %>%
       summarise(Retrasos=sum(dep_delay,na.rm=TRUE)) %>%
       arrange(desc(Retrasos)),1) 


#3 ¿Cuál es la mejor aerolínea? UA
head(flights %>%
       rename( Aerolinea = carrier) %>%
       group_by(Aerolinea) %>%
       summarise(llegadas_on_time=sum(arr_time,na.rm=TRUE)) %>%
       arrange(desc(llegadas_on_time)),1)


#4 ¿Cuál es el destino más frecuente? ORD 
head(flights %>%
       group_by(dest) %>%
       count(topdestino=count(flights,na.rm=TRUE)) %>%
       arrange(desc(n)) %>%
       rename(Cantidad_Vuelos = n, Aeropuerto =dest) %>%
       select (Aeropuerto, Cantidad_Vuelos),1)


#5 ¿De dónde venían la mayor cantidad de vuelos? EWR
head(flights %>%
       group_by(origin) %>%
       count(topllegadas=count(flights,na.rm=TRUE)) %>%
       arrange(desc(n)) %>%
       rename(Cantidad_Vuelos = n, Origen = origin) %>%
       select(Origen, Cantidad_Vuelos),1)


#6  ¿Cuáles son las 10 rutas más transitadas? JFL-LAX

head(flights %>% 
       mutate(Ruta = paste0 (origin,' -> ', dest))  %>% 
       group_by(Ruta)  %>% 
       count(TopRutas=count(flights,na.rm=TRUE)) %>%
       arrange(desc(n)) %>%
       rename(Cantidad_Vuelos = n) %>%
       select(Ruta,Cantidad_Vuelos),10)


#7  ¿Qué día de la semana es el peor para viajar? Thursday

head(flights %>% 
       mutate(Fecha = weekdays(as.Date(time_hour))) %>%
       group_by(Fecha) %>%
       summarise(Retrasos_Salida = sum(dep_delay,na.rm=TRUE), Retrasos_Llegada = sum(arr_delay,na.rm=TRUE) , Total_Reatrasos = sum(Retrasos_Salida)+sum(Retrasos_Llegada)) %>%
       arrange(desc(Total_Reatrasos)),1)

