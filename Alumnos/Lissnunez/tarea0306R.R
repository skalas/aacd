#1.¿Cuál fue el peor día para viajar en 2013?
#1.1.Razonamiento & Respuesta: Si el “Peor” es en función del dia que tuvo mayor retraso en la salida fue el 9 de Enero con 1,301 minutos de retraso.'

flights %>%
  filter (year== 2013) %>%
  select(year, month, day,dep_delay)%>%
  arrange(desc(dep_delay))%>%
  top_n(1)

#1.2.Razonamiento & Respuesta: Si el “Peor” es en función del dia que tuvo mayor retraso en la llegada fue el 9 de Enero con 1,272 minutos de retraso.

flights %>%
  filter (year == 2013) %>%
  select(year, month, day,arr_delay)%>%
  arrange(desc(arr_delay))%>%
  top_n(1)

# 2. ¿Cuál es la peor aerolinea con respecto a retrasos?
#2.1.	Razonamiento &Respuesta: Si el “Peor” es en función del  mayor retraso en las salidas históricas es F9  

flights %>%
  group_by(carrier)%>%
  summarise(retrasos_salida = mean(dep_delay, na.rm=TRUE))%>%
  arrange(desc(retrasos_salida))%>%
  top_n(1)

#2.2.	Razonamiento &Respuesta: Si el “Peor” es en función del  mayor retraso en las llegadas históricas es  F9

flights %>%
  group_by(carrier)%>%
  summarise(retrasos_llegada = mean(arr_delay,na.rm=TRUE))%>%
  arrange(desc(retrasos_llegada))%>%
  top_n(1)

#3. ¿Cuál es la mejor aerolínea?
#3.1.	Razonamiento & Respuesta: Si el “Mejor” es en función del menor retraso en las llegadas históricas es  AS con un promedio de llegada de 9.93 minutos de anticipación 
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_llegada= mean(arr_delay, na.rm=TRUE))%>%
  arrange(retrasos_llegada)%>%
  slice_min(retrasos_llegada)

#3.2.3.2.	Si el “Mejor” es en función del menor retraso en las salidas históricas es  US con un promedio de llegada de 3.78 minutos de anticipación 
flights %>%
  group_by(carrier)%>%
  summarise(retrasos_salida= mean(dep_delay, na.rm=TRUE))%>%
  arrange(retrasos_salida)%>%
  slice_min(retrasos_salida)

#4.	¿Cuál es el destino más frecuente?
# Respuesta: ORD
flights %>%
  group_by(dest)%>%
  count( dest ) %>%
  arrange(desc(n))

#5.	¿De dónde venían la mayor cantidad de vuelos?
#Respuesta:EWR

flights %>%
  group_by(origin)%>%
  count( origin ) %>%
  arrange(desc(n))

#6. ¿Cuáles son las 10 rutas más transitadas?
#Respuesta: Pendiente

#7.	¿Qué día de la semana es el peor para viajar?
#7.1.	Razonamiento & Respuesta: Si el “peor” es en función al dia de la semana con mayor retraso en las llegadas históricas es el jueves con 11 minutos de retrasos en promedio

flights %>%
  mutate(fecha = as.Date(time_hour), dia_semana= weekdays(fecha)) %>%
  group_by(dia_semana)%>%
  summarise(minutos_promedio_retrasos= mean(arr_delay, na.rm=TRUE)) %>%
  arrange(minutos_promedio_retrasos)%>%
  top_n(1)

#7.2.	Si el “peor” es en función al dia de la semana con mayor retraso en las salidas históricas es el jueves con 15 minutos de retrasos en promedio
flights %>%
  mutate(fecha = as.Date(time_hour), dia_semana= weekdays(fecha)) %>%
  group_by(dia_semana)%>%
  summarise(minutos_promedio_retrasos= mean(dep_delay, na.rm=TRUE)) %>%
  arrange(minutos_promedio_retrasos)%>%
  top_n(1)







