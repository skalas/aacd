library(tidyverse)
library(readxl)
excel_sheets('data/Base MACO Tool values.xlsx')
data <- read_excel('data/Base MACO Tool values.xlsx', sheet='BD', skip=4,
                   col_types = c("text","text","text","text",
                                 "text","text","text","text",
                                 "text","text","text","text",
                                 "text","text","text","text",
                                 "numeric","numeric","numeric","numeric",
                                 "numeric","numeric","numeric","numeric",
                                 "numeric","numeric","numeric","numeric",
                                 "numeric","numeric","numeric","numeric",
                                 "numeric"))

## "Legaliza" los nombres del data frame :) .
names(data) <- make.names(names(data))

data %>%
    select(-(New.GTO:VLC...33)) %>%
    saveRDS(file='./AppEjemplo/datos/data.Rdata')

data %>%
    select(-(New.GTO:VLC...33)) %>%
    saveRDS(file='./data.Rdata')


data %>%
    select(Country) %>%
    group_by(Country) %>%
    summarise(n=n())

datos <- readRDS('./AppEjemplo/datos/data.Rdata')
