################################### Tarea Vuelos ###############################

# Integrantes: 
# 1. Melisa Villada
# 2. Estephany Ayala
#  3. Daniel Rozo Isaza
              
################################# Limpiar y Cargar Librerias ###################

# install.packages("lubridate")

rm(list = ls())

library(nycflights13)
library(tidyverse)
library(dplyr)
library(lubridate)

################################# Codigo #######################################

df <- flights

#   ¿Cuál fue el peor día para viajar en 2013?

df %>% 
  filter(year == 2013) %>%
  select(month:dep_delay) %>%
  group_by(month, day) %>% 
  summarise(dep_delay = mean(dep_delay, na.rm = TRUE)) %>%
  arrange(desc(dep_delay)) %>% 
  head(1)

#   ¿Cuál es la peor aerolinea con respecto a retrasos?

df %>% 
  group_by(carrier) %>%
  summarise(dep_delay = mean(dep_delay, na.rm = T)) %>%
  arrange(desc(dep_delay)) %>%
  head(1)

#   ¿Cuál es la mejor aerolínea?

df %>% 
  group_by(carrier) %>%
  summarise(arr_delay = mean(arr_delay, na.rm = T)) %>%
  arrange(arr_delay) %>%
  head(1)

#   ¿Cuál es el destino más frecuente?

df %>% 
  group_by(dest) %>%
  summarise(n = n()) %>% 
  arrange(desc(n)) %>%
  head(1) 


#   ¿De dónde venían la mayor cantidad de vuelos?
df %>% 
  group_by(origin) %>%
  summarise(n = n()) %>% 
  arrange(desc(n)) %>% 
  head(5) %>% 
  print()

#   ¿Cuáles son las 10 rutas más transitadas?

df %>% 
  select(origin, dest) %>%
  group_by(origin, dest) %>% 
  summarise(n = n()) %>% 
  arrange(desc(n)) %>% 
  head(10)

#   ¿Qué día de la semana es el peor para viajar?

df %>%
  select(time_hour, dep_delay) %>% 
  mutate(date = as.Date(as.POSIXct(time_hour))) %>% 
  mutate(week_day = wday(date, label = T)) %>% 
  group_by(week_day) %>% 
  summarise(dep_delay = mean(dep_delay, na.rm = T)) %>% 
  arrange(desc(dep_delay)) %>% 
  head(1)
  






