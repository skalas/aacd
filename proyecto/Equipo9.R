################### TAREA: Ejercicios con base flight
library(dplyr)
library(nycflights13)
library(ggplot2)

flights

#######Entendiendo la información de la base
?flights
tail(flights)


######¿Cuál fue el peor día para viajar?
pregunta1 <- paste("¿Cuál fue el peor día para viajar?")
####¿Qué empeora un vuelo? : los retrasos

flights ['dep_delay' ]
flights ['arr_delay' ]
summary (flights$dep_delay)
summary (flights$arr_delay)

#### Encontrando la moda del retraso de los vuelos
retrasosalida <- flights %>%
  filter(  dep_delay > 0)
    frecuencias1 <- data.frame(table(retrasosalida$dep_delay))
    moda <- frecuencias1[which.max(frecuencias1$Freq),1]
    max(frecuencias1$Freq,na.rm=FALSE)
    paste("La moda de la variable retraso salida es", moda)
    
retrasollegada <- flights %>%
  filter(  arr_delay > 0)
  frecuencias2 <- data.frame(table(retrasollegada$arr_delay))
    moda <- frecuencias2[which.max(frecuencias2$Freq),1]
    max(frecuencias2$Freq,na.rm=FALSE)
    paste("La moda de la variable retraso llegada es", moda)

    
#### Al parecer la moda es que los vuelos lleguen a tiempo
##Comprobemoslo con un histograma
    flights %>%
      select (dep_delay)%>%
      ggplot (aes(dep_delay))+
      geom_histogram()   

##### Si definimos el peor día como el día con más vuelos retrasados:
####Analicemos el comportamiento promedio por día 
    flights %>%
      ##Como seres no racionales y rencorosos solo nos centraremos en los retrasos
      filter( arr_delay > 0, dep_delay > 0) %>%  
      group_by (year,month,day) %>%
      summarise (retrasos_salida= mean (dep_delay,na.rm=TRUE)
                 ,retrasos_llegada= mean (arr_delay,na.rm=TRUE)
                 ,retraso_total = mean (dep_delay + arr_delay, na.rm=TRUE)
                 #,VueloRetrasados <- count(dep_delay) + count(arr_delay)
                 )  %>%
      arrange (desc(retraso_total)) 
    
paste("El peor día para viajar fue el 12 de septiembre, debido a que su tiempo promedio de retraso por vuelo fue de 239 minutos")
respuesta1 <- paste("El peor día para viajar fue el 12 de septiembre, debido a que su tiempo promedio de retraso por vuelo fue de 239 minutos")

######¿Cual es la peor aerolinea con respecto a Retrasos?
pregunta2 <- paste ("¿Cual es la peor aerolinea con respecto a Retrasos?")
####Sigamos la metodología usada con el peor día, ahora con las aerolineas
    flights %>%
      filter( arr_delay > 0, dep_delay > 0) %>%
      group_by (carrier) %>%
      summarise (retrasos_salida= mean (dep_delay,na.rm=TRUE)
                 ,retrasos_llegada= mean (arr_delay,na.rm=TRUE)
                 ,retraso_total = mean (dep_delay + arr_delay, na.rm=TRUE) ) %>%
      arrange (desc(retraso_total)) 
    paste("La peor aerolia con respecto a retrasos es la HA")
respuesta2 <- paste("La peor aerolia con respecto a retrasos es la HA")
    
  
######¿Cual es la mejor aerolinea?
pregunta3 <- paste("¿Cuál es la mejor aerolinea?")
####Sigamos la metodología usada con el peor día, ahora con las aerolineas
    flights %>%
      ##Para este caso si consideraremos tanto los atrasos como los adelantos
      #porque es más justo evaluar a la aerolínea contando ambos factores
      #filter( arr_delay <= 0, dep_delay <= 0) %>%
      group_by (carrier) %>%
      summarise (retrasos_salida= mean (dep_delay,na.rm=TRUE)
                 ,retrasos_llegada= mean (arr_delay,na.rm=TRUE)
                 ,retraso_total = mean (dep_delay + arr_delay, na.rm=TRUE) ) %>%
      arrange (retraso_total)   
  
####Imaginaramos si solo tomaramos los vuelos que salen antes  
    flights %>%
      ##Para este caso consideraremos solo los atrasos por curiosidad
      filter( arr_delay < 0, dep_delay < 0) %>%
      group_by (carrier) %>%
      summarise (retrasos_salida= mean (dep_delay,na.rm=TRUE)
                 ,retrasos_llegada= mean (arr_delay,na.rm=TRUE)
                 ,retraso_total = mean (dep_delay + arr_delay, na.rm=TRUE) ) %>%
      arrange (retraso_total) 
    paste("La mejor aerolinea en ambos análisis que muestra ser la mejor es la AS")
respuesta3 <- paste("La mejor aerolinea en ambos análisis que muestra ser la mejor es la AS")
    
######¿Cuál destino más visitado?
pregunta4 <- paste("¿Cuál destino más visitado?")
####Revisemos las frecuencias de vuelos
  flights
  flights$dest <- factor(flights$dest)  
  summary(flights$dest)
    
  vuelospordestino <- data.frame(table(flights$dest))
      moda <- vuelospordestino[which.max(vuelospordestino$Freq),1]
      #max(frecuencias3$Freq,na.rm=FALSE)
    paste("El destino más visitado es", moda,
          " con ",max(vuelospordestino$Freq,na.rm=FALSE)
          , "vuelos")
      
 respuesta4 <- paste("El destino más visitado es", moda,
                     " con ",max(vuelospordestino$Freq,na.rm=FALSE)
                     , "vuelos")
         
######¿De dónde venían la mayor cantidad de vuelos?
pregunta5 <- paste ("¿De dónde venían la mayor cantidad de vuelos?")
####Utilicemos la misma lógica pasada
  flights
  flights$origin <- factor(flights$origin)  
  summary(flights$origin)
##Como son pocos origenes desde aqui sabemos la respuesta, pero 
#usemos el método anterior
      
  frecuencias4 <- data.frame(table(flights$origin))
    moda <- frecuencias4[which.max(frecuencias4$Freq),1]
      #max(frecuencias3$Freq,na.rm=FALSE)
    paste("El origen más común es", moda,
          " con ",max(frecuencias4$Freq,na.rm=FALSE)
          , "vuelos")
respuesta5 <- paste("El origen más común es", moda,
                    " con ",max(frecuencias4$Freq,na.rm=FALSE)
                    , "vuelos")

      
#######¿Cuáles son las 10 rutas más transitadas?
pregunta6 <- paste("¿Cuáles son las 10 rutas más transitadas?")
####Primero revisemos si existe una variable que lo indique
?flights
      flights
      
####Al no contar con una variable de ruta, debemos crear una
ruta <- paste(flights$origin,flights$dest, sep= "")
rutaID <- factor(ruta)
summary(rutaID)


###pENDIENTE ARREGLAR ESTA PARTE
rutastransitadas <- data.frame(table(ruta))
  moda <- rutastransitadas[which.max(rutastransitadas$Freq),1]
  #max(rutastransitadas$Freq,na.rm=FALSE)
paste("La ruta más transitada es la ruta ", moda,
        " con ",max(rutastransitadas$Freq,na.rm=FALSE)
        , "vuelos")
respusta6 <- paste("La ruta más transitada es la ruta ", moda,
                   " con ",max(rutastransitadas$Freq,na.rm=FALSE)
                   , "vuelos")

      
######¿Qué día de la semana es el peor para viajar?
pregunta7 <- paste("¿Qué día de la semana es el peor para viajar?")
####Comencemos a revisar
flights %>%
  mutate(
    weekdays_factor=factor(weekdays(as.POSIXct(flights$time_hour)))
  ) %>%
  group_by(weekdays_factor) %>%
  summarise(
    count = n(),
    delay = mean(dep_delay, na.RM=TRUE),
  )
paste ("El peor día es lunes, pues es el que más días con retraso acumula")
respuesta7 <- paste ("El peor día es lunes, pues es el que más días con retraso acumula")

#### El peor día es lunes, pues es el que más días con retraso acumula




######RESPUESTAS
paste("Preguntas & Respuestas")
pregunta1
respuesta1
pregunta2
respuesta2
pregunta3
respuesta3
pregunta4
respuesta4
pregunta5
respuesta5
pregunta6
respusta6
pregunta7
respuesta7
      
      
      
      
      